//
// Created by Jonathan YEUNG on 31/8/2019.
// DemoSwift
// Copyright © 2019 THS. All rights reserved.
//

import SwiftUI

struct LoginView: View {
//    @EnvironmentObject var loginViewModel: LoginViewModel
    @State private var username: String = ""
    @State private var password: String = ""
    @State var counter: Int? = 0
    
    var body: some View {
        NavigationView {
            VStack {
                Spacer()
                Image("btn_map_release")
                Text("Login Page")
                Spacer()
                
                HStack {
                    Spacer()
                    TextField("Enter a username", text: $username)
                        .textContentType(UITextContentType.username)
                        .colorScheme(ColorScheme.dark)
                    Spacer()
                }
                
                HStack {
                    Spacer()
                    SecureField("Enter a password", text: $password)
                        .textContentType(UITextContentType.password)
                        .colorScheme(ColorScheme.dark)
                    Spacer()
                }
                
                HStack {
                    Spacer()
                    NavigationLink(destination: MainViewController(), tag: 1, selection: $counter) {
                        Button(action: submit) {
                            Text("Submit")
                        }
                            .foregroundColor(Color.gray)
                            .frame(width: CGFloat(100), height: CGFloat(30), alignment: Alignment.center)
                    }
                    Spacer()
                }
                
                Spacer()
                Spacer()
            }
            .navigationBarTitle(Text("Login"))
            .navigationBarHidden(true)
        }
    }
    
    func submit() {
        if self.username == "jonathan" && self.password == "1234" {
            self.counter = 1
        }
    }
}
