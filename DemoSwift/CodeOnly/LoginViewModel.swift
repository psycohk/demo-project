//
// Created by Jonathan YEUNG on 1/9/2019.
// DemoSwift
// Copyright © 2019 THS. All rights reserved.
// 

import Combine

class LoginViewModel : ObservableObject {
    @Published var username = ""
    @Published var password = ""
}
