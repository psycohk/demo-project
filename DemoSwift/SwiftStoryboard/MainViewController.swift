//
// Created by Jonathan YEUNG on 22/9/2019.
// DemoSwift
// Copyright © 2019 THS. All rights reserved.
// 

import UIKit
import SwiftUI

extension MainViewController : UIViewControllerRepresentable {
    public typealias UIViewControllerType = MainViewController

    func makeUIViewController(context: UIViewControllerRepresentableContext<MainViewController>) -> MainViewController {
        let viewController : MainViewController = UIStoryboard(name: "MainStoryboard", bundle: nil).instantiateViewController(withIdentifier: "Main") as! MainViewController
        return viewController
    }
    
    func updateUIViewController(_ uiViewController: MainViewController, context: UIViewControllerRepresentableContext<MainViewController>) {
        //
    }
}

final class MainViewController : UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
