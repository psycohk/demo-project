//
// Created by Jonathan YEUNG on 23/9/2019.
// DemoSwift
// Copyright © 2019 THS. All rights reserved.
// 

import UIKit

class SecondViewController : UIViewController {
    @IBOutlet weak var button : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onSubmitButton(sender : UIButton) {
        print("Hello")
    }
}
